Searches for users through the explore tab.

`GET /search/users?q=jeffalo`

Searches for posts with the keywords through the explore tab.

`GET /search/posts?q=joe+mama`

(You can add in a `?page` parameter to specify which page of users or posts you want. Example: `/search/posts?q=e&page=2`)
