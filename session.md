**NOTE: Your session is required for most endpoints to function properly.**

***

Gets the current logged in user's session.

`GET /session`

Logs you into an account.

`POST /session`

Logs you out of the currently logged in account.

`DELETE /session`
